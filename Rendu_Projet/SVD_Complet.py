import math
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

np.set_printoptions(linewidth=240)

A = np.array ([
    [3, 97.8, 119, 2.6, 7.4, 0.2, 1.8, 2.2, 53, 34, 28.3, 21],
    [0.2, 80.5, 121, 2.1, 5.3, 0.5, 1.4, 1.4, 47.4, 20.5, 27.8, 19.9],
    [0.4, 6.1, 67, 4.2, 9.9, 3, 1.5, 2.5, 10.6, 21, 15.1, 23.1],
    [7.3, 106.4, 129, 1.9, 14.7, 1.1, 2.1, 3.7, 45.9, 12.5, 22.1, 29.9],
    [4.6, 170.6, 79, 1, 26.4, -4.4, 1.4, 11.8, 27.6, 23, 26, 31],
    [6.7, 69.3, 98, 2.4, 26.2, -1.4, 1.4, 4.9, 32.7, 35, 22.7, 27],
    [3.7, 86, 108, 2.2, 10.6, 0.1, 2, 2, 45.4, 34, 30.8, 19.3],
    [2.1, 120.7, 100, 3.3, 11.7, -1, 1.4, 4.6, 35.6, 33, 27.8, 24.5],
    [4.5, 71.1, 94, 3.1, 14.7, -3.5, 1.4, 12, 20.7, 10, 18.4, 23.5],
    [0.9, 18.3, 271, 2.9, 5.3, 0.5, 1.5, 2.3, 50.7, 22, 20.1, 16.8], 
    [2.9, 70.9, 85, 3.2, 7, 1.5, 1.5, 4, 25, 35, 18.9, 21.4],
    [3.6, 65.5, 131, 2.8, 6, -0.6, 1.8, 1.6, 55.7, 34, 28.4, 15.7],
    [2.5, 72.4, 129, 2.6, 4.9, 0.7, 1.4, 1.7, 45.6, 34, 28.2, 16.9],
    [4.9, 108.1, 77, 2.8, 17.6, -1.9, 1.4, 6, 14.8, 25, 24.3, 24.4],
    [5.1, 46.9, 84, 2.8, 10.2, -2, 1.6, 4.9, 18.5, 20, 21.5, 19.3],
    [3.3, 43.3, 73, 3.7, 14.9, 1.1, 1.5, 2.1, 10.1, 19, 16, 20.6],
    [1.5, 49, 114, 3.2, 7.9, 0.3, 1.8, 1.6, 46.8, 26, 26.3, 17.9]],
    dtype=np.float64)

m, n = A.shape

#Centrage des donnees
Acentre = (A - np.mean(A,axis=0))

#Adimensionnement des donnees
Aadim = Acentre /np.std(A,axis=0)

#Matrice de reflexion de Householder
def reflecteur (p, v) :
        n = v.shape[0]
        F = np.eye (n) - 2 * np.outer (v,v)
        Q = np.eye (p, dtype=np.float64)
        Q [p-n:p,p-n:p] = F
        return Q

#Calcul de matrice bidiagonale B  
B = np.copy (Aadim) 
VL=[]
VR=[]
for i in range(0,n-2):
 x = B[i:m,i]
 v = np.copy(x)
 v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
 v = (1/nla.norm(v,2))*v
 VL.append(v)
 Q = reflecteur (m, v)
 B = np.dot (Q, B)

 x = B[i,i+1:n]
 v = np.copy(x)
 v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
 v = (1/nla.norm(v,2))*v
 VR.append(v)
 Q = reflecteur (n, v)
 B = np.dot (B, Q)

for i in range(n-2,n):
 x = B[i:m,i]
 v = np.copy(x)
 v[0] = v[0] + np.sign(v[0])*nla.norm(x,2)
 v = (1/nla.norm(v,2))*v
 VL.append(v)
 Q = reflecteur (m, v)
 B = np.dot (Q, B)

#On supprime la ligne de zéros de B, qui est donc de dimension n × n.
B = B[0:n,0:n]

#On construit les matrices H et P.
H = np.zeros ([2*n, 2*n], dtype=np.float64)
H [0:n,n:2*n] = np.transpose(B)
H [n:2*n,0:n] = B

P = np.zeros ([2*n,2*n], dtype=np.float64)
for i in range (0,n) :
    P[i,2*i] = 1
    P[n+i,2*i+1] = 1

#On construit la matrice tridiagonale T.
T = np.dot (np.transpose(P), np.dot (H, P))

#On calcule maintenant les 2 n valeurs propres de T et un vecteur propre de longueur 1 pour
#chaque valeur propre grâce à la fonction eigh_tridiagonal de scipy.linalg. Comme T
#est symétrique, les vecteurs propres sont orthogonaux deux–à–deux.

d = np.zeros (2*n, dtype=np.float64)

e = np.array ([ T[i+1,i] for i in range (0,2*n-1) ], dtype=np.float64)

eigvals, eigvecs = nla.eigh_tridiagonal(d, e)

#On extrait les valeurs propres positives (qui sont les valeurs singulières de A) et les vecteurs
#propres correspondants.

Lambda = eigvals [n:2*n]

Q = eigvecs [:,n:2*n]

#On construit la matrice intermédiaire Y = √2 P Q
Y = np.sqrt(2) * np.dot (P, Q)

#On obtient une décomposition en valeurs singulières B = Unew Σnew VTnew de B en découpant Y
#en deux matrices. On rajoute une ligne de zéros à Unew pour revenir aux dimensions de la
#matrice initiale A.

newVt = np.transpose (Y[0:n,:])

newU = np.zeros ([m,n], dtype=np.float64)
newU[0:n,:] = Y[n:2*n,:]


newSigma = np.array (np.diag (Lambda), dtype=np.float64)

#En remplaçant B par sa factorisation B = Unew Σnew VTnew dans A = QL B QR, on voit qu’il
#suffit de multiplier Unew à gauche par QL et VTnew à droite par QR pour obtenir une décomposition en valeurs singulières de A. 
#Les instructions ci-dessous effectuent ces multiplications
#à partir des tableaux VL et VR en tirant parti du fait que les matrices de réflexions sont
#symétriques.

for i in range (n-1, -1, -1):
     Q = reflecteur (m, VL[i])
     newU = np.dot (Q, newU)
    
for i in range (n-3, -1, -1):
    Q = reflecteur (n, VR[i])
    newVt = np.dot (newVt, Q)
	
newA = np.dot (newU, np.dot (newSigma, newVt))
print(newA)
                                  
for i in range (1,newSigma.shape[1]):
    newSigmaDiag[i]=newSigma[i,i]

#On fait cette manipulation vu que les valeurs singulieres apparaissent par ordre croissant, contrairement a la convention classique

#On récupère les indidces des deux plus grandes vlauers propre (en valeur absolue)
indices = (-abs(newSigmaDiag)).argsort()[:2] 

#Extraction des vecteurs propres associés aux deux plus grandes valeurs propres
newVt=np.transpose(newVt)

V1=np.array(newVt[:,indices[0]]) 
V2=np.array(newVt[:,indices[1]]) 


#Calcul des coordonnées des points 
P=np.dot(Aadim,np.transpose((V1,V2)))

#Cette boucle nous permet d'afficher le graphe correctement, sans cette methode
#On retrouve le  graphe symetrique de celui qu'on souhaite obtenir. 

for i in range(m-1):
    P[i][0]=-P[i][0]
    P[i][1]= -P[i][1]

lignes = ['BE', 'DE', 'EE', 'IE', 'EL', 'ES', 'FR', 'IT', 'CY', 'LU', 'MT', 'NL', 'AT', 'PT', 'SL', 'SK', 'FI']



plt.scatter (P[:,0],P[:,1])
for i in range (0,m) :
    plt.annotate(lignes[i],P[i,:])
plt.show()
    