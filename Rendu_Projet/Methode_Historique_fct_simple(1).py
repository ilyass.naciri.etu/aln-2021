import math
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

np.set_printoptions(linewidth=240)

A = np.array ([
    [3, 97.8, 119, 2.6, 7.4, 0.2, 1.8, 2.2, 53, 34, 28.3, 21],
    [0.2, 80.5, 121, 2.1, 5.3, 0.5, 1.4, 1.4, 47.4, 20.5, 27.8, 19.9],
    [0.4, 6.1, 67, 4.2, 9.9, 3, 1.5, 2.5, 10.6, 21, 15.1, 23.1],
    [7.3, 106.4, 129, 1.9, 14.7, 1.1, 2.1, 3.7, 45.9, 12.5, 22.1, 29.9],
    [4.6, 170.6, 79, 1, 26.4, -4.4, 1.4, 11.8, 27.6, 23, 26, 31],
    [6.7, 69.3, 98, 2.4, 26.2, -1.4, 1.4, 4.9, 32.7, 35, 22.7, 27],
    [3.7, 86, 108, 2.2, 10.6, 0.1, 2, 2, 45.4, 34, 30.8, 19.3],
    [2.1, 120.7, 100, 3.3, 11.7, -1, 1.4, 4.6, 35.6, 33, 27.8, 24.5],
    [4.5, 71.1, 94, 3.1, 14.7, -3.5, 1.4, 12, 20.7, 10, 18.4, 23.5],
    [0.9, 18.3, 271, 2.9, 5.3, 0.5, 1.5, 2.3, 50.7, 22, 20.1, 16.8], 
    [2.9, 70.9, 85, 3.2, 7, 1.5, 1.5, 4, 25, 35, 18.9, 21.4],
    [3.6, 65.5, 131, 2.8, 6, -0.6, 1.8, 1.6, 55.7, 34, 28.4, 15.7],
    [2.5, 72.4, 129, 2.6, 4.9, 0.7, 1.4, 1.7, 45.6, 34, 28.2, 16.9],
    [4.9, 108.1, 77, 2.8, 17.6, -1.9, 1.4, 6, 14.8, 25, 24.3, 24.4],
    [5.1, 46.9, 84, 2.8, 10.2, -2, 1.6, 4.9, 18.5, 20, 21.5, 19.3],
    [3.3, 43.3, 73, 3.7, 14.9, 1.1, 1.5, 2.1, 10.1, 19, 16, 20.6],
    [1.5, 49, 114, 3.2, 7.9, 0.3, 1.8, 1.6, 46.8, 26, 26.3, 17.9]],
    dtype=np.float64)
    
m,n=A.shape

#Centrage des donnees
Acentre = (A - np.mean(A,axis=0))

#Adimensionnement des donnees
Aadim = Acentre /np.std(A,axis=0)

#Matrice de covariance

C=(1/(n-1))*np.dot(np.transpose(Aadim),Aadim)

# factorisation de Schur
T,Q= nla.schur(C)

# Algorithme QR avec comme matrice initiale la matrice C 
#Cet algorithme converge vers la matrice Q1
C1=C
for k in range (0,100) :
    Q, R = nla.qr (C1)
    C1 = np.dot (R, Q)

# On essaie de construire la diagonalisation en utilisant l'algo de puissance inverse
Lambda = np.zeros([n,n])
X = np.empty ([n,n])
for i in range(0,n) :
    mu = C1[i,i]     # Des approximations des valeurs propres sont sur la diagonale de Q1
    # puissance inverse -> v
    v = np.array([5,9,7,2,4,6,-9.8,9.9,1,2,5,89])
    M = C - mu*np.eye(n)
    P, L, U = nla.lu (M)
    for k in range (0, 10) :
        v = np.dot (v, P)  # v = P**T . v
        w = nla.solve_triangular (L, v, lower=True)
        w = nla.solve_triangular (U, w, lower=False)
        v = (1 / nla.norm(w, 2)) * w
    X[:,i] = v
    Lambda[i,i] = np.dot(v, np.dot (C, v)) # le quotient de Rayleigh

L=np.diag(Lambda)#On stocke les valeurs propres dans L

#Meme methode d'extraction des valeurs et vecteurs propres

indices = (-abs(L)).argsort()[:2]

Qi=np.array(X[:,indices[0]]) #on extrait le vecteur propre correspondant a la valeur propre la plus grande
Qj=np.array(X[:,indices[1]])

P=np.dot(Aadim,np.transpose((Qi,Qj)))

lignes = ['BE', 'DE', 'EE', 'IE', 'EL', 'ES', 'FR', 'IT', 'CY', 'LU', 'MT', 'NL', 'AT', 'PT', 'SL', 'SK', 'FI']
plt.scatter (P[:,0], P[:,1])
for i in range (0,m) :
    plt.annotate (lignes[i], P[i,:])
	
plt.show()


