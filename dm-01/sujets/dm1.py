import numpy as np
import scipy.linalg as nla

#Question 1

def f(x):
    return x**2-4

def df(x):
   return 2*x

u=[3.0]

for i in range (6):
    u.append(u[i]-(f(u[i])/df(u[i])))
print (u)

# si u0<0  la suite converge vers -2

def f1(x):
    return  (x-2)**2

def df1(x):
    return 2*x-4

u=[3.0]

for i in range (6):
    u.append(u[i]-(f1(u[i])/df1(u[i])))
print (u)

#  La convergence vers 2 est moins rapide

def f2(x):
    return  x**2-3

def df2(x):
    return 2*x

u=[3.0]

for i in range (6):
    u.append(u[i]-(f2(u[i])/df2(u[i])))
print (u)

#Question 2


def f1(x,y):
  return x**2+2*x*y-1


def f2(x,y):
  return (x**2)*(y**2)-y-3


#f(x,y)=(f1(x,y),f2(x,y))
def f(x,y):
  return np.array([f1(x,y),f2(x,y)])

print (f(2,-3/4))


def jacobien(x,y):
     return np.array ([[2*x+2*y,2*x],[2*x*(y**2),2*y*(x**2)-1]])


def mf(x,y):
   return np.array([-f1(x,y),-f2(x,y)])

u = np.array ([3.0,-1])
print (u)

for i in range(6) :
    J = jacobien(u[0], u[1])
    mof = mf(u[0], u[1])

    Q,R = nla.qr(J)
    x = nla.solve_triangular(R, np.dot(np.transpose(Q),mof),lower=False)
    u[0]=u[0]+x[0]
    u[1]= u[1]+x[1]
    print(u)

